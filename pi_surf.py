# -*- coding: utf-8 -*-
#
# 2018-08-01 Bas van der Vlies <bas.vandervlies@surfsara.nl>
#            Add TIQR support, remove offline code
# 2016-08-31 Cornelius Kölbel <cornelius.koelgel@netknights.it>
#            Add header user-agent to request
# 2015-03-04 Cornelius Kölbel <cornelius.koelbel@netknights.it>
#            Add normal challenge/response support
# 2016-03-03 Brandon Smith <freedom@reardencode.com>
#            Add U2F challenge/response support
# 2015-11-06 Cornelius Kölbel <cornelius.koelbel@netknights.it>
#            Avoid SQL injections.
# 2015-10-17 Cornelius Kölbel <cornelius.koelbel@netknights.it>
#            Add support for try_first_pass
# 2015-04-03 Cornelius Kölbel  <cornelius.koelbel@netknights.it>
#            Use pbkdf2 to hash OTPs.
# 2015-04-01 Cornelius Kölbel  <cornelius.koelbel@netknights.it>
#            Add storing of OTP hashes
# 2015-03-29 Cornelius Kölbel, <cornelius.koelbel@netknights.it>
#            Initial creation
#
# (c) Cornelius Kölbel
# Info: http://www.privacyidea.org
#
# This code is free software; you can redistribute it and/or
# modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
# License as published by the Free Software Foundation; either
# version 3 of the License, or any later version.
#
# This code is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU AFFERO GENERAL PUBLIC LICENSE for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__doc__ = """This is the PAM module to be used with python-pam with the
privacyIDEA authentication system.

The code is tested in test_pam_module.py
"""

import json
import requests
import syslog
import time
import traceback


def _get_config(argv):
    """
    Read the parameters from the arguments. If the argument can be split with a
    "=", the parameter will get the given value.

    :param argv:
    :return: dictionary with the parameters
    """
    config = {}
    for arg in argv:
        argument = arg.split("=")
        if len(argument) == 1:
            config[argument[0]] = True
        elif len(argument) == 2:
            config[argument[0]] = argument[1]
    return config


class Authenticator(object):

    def __init__(self, pamh, config):
        self.pamh = pamh
        self.user = pamh.get_user(None)
        self.service = pamh.service
        self.URL = config.get("url", "https://localhost")
        self.sslverify = not config.get("nosslverify", False)
        cacerts = config.get("cacerts")
        # If we do verify SSL certificates and if a CA Cert Bundle file is
        # provided, we set this.
        if self.sslverify and cacerts:
            self.sslverify = cacerts
        self.realm = config.get("realm")
        self.debug = config.get("debug")
        self.push_timeout = config.get("push_timeout", 30)

    def make_request(self, data, endpoint="/validate/check"):
        # add a user-agent to be displayed in the Client Application Type
        headers = {'user-agent': 'PAM/2.15.0'}
        response = requests.post(self.URL + endpoint, data=data,
                                 headers=headers, verify=self.sslverify)

        json_response = response.json
        if callable(json_response):
            syslog.syslog(syslog.LOG_DEBUG, "requests > 1.0")
            json_response = json_response()

        return json_response

    def authenticate(self, password):
        rval = self.pamh.PAM_SYSTEM_ERR
        if self.debug:
            syslog.syslog(syslog.LOG_DEBUG, "Authenticating %s against %s" %
                          (self.user, self.URL))
        data = {"user": self.user,
                "pass": password}
        if self.realm:
            data["realm"] = self.realm

        json_response = self.make_request(data)

        result = json_response.get("result")
        auth_item = json_response.get("auth_items")
        detail = json_response.get("detail") or {}
        serial = detail.get("serial", "T%s" % time.time())
        tokentype = detail.get("type", "unknown")
        if self.debug:
            syslog.syslog(syslog.LOG_DEBUG,
                          "%s: result: %s" % (__name__, result))
            syslog.syslog(syslog.LOG_DEBUG,
                          "%s: detail: %s" % (__name__, detail))

        if result.get("status"):
            if result.get("value"):
                if self.debug:
                    syslog.syslog(syslog.LOG_DEBUG,
                            "%s: success for tokentype: %s" % (__name__, tokentype))
                rval = self.pamh.PAM_SUCCESS
            else:
                transaction_id = detail.get("transaction_id")

                if transaction_id:
                    attributes = detail.get("attributes") or {}
                    message = detail.get("message").encode("utf-8")
                    if tokentype in [ 'tiqr' ]:
                        return self.tiqr_challenge_response(
                                 password, transaction_id,
                                 message, attributes)
                    if tokentype in [ 'push' ]:
                        return self.push_challenge_response(
                                 password, transaction_id,
                                 message, attributes)
                    elif tokentype in [ 'totp']:
                        return self.totp_challenge_response(
                                 password, transaction_id,
                                 message, attributes)
                else:
                    rval = self.pamh.PAM_AUTH_ERR
        else:
            syslog.syslog(syslog.LOG_ERR,
                          "%s: %s" % (__name__,
                                      result.get("error").get("message")))

        return rval


    def check_push_response(self, password, transaction_id, message):
        rval = self.pamh.PAM_AUTH_ERR

        try:
            max_time = int(self.push_timeout)
        except ValueError, detail:
            syslog.syslog(syslog.LOG_ERR,
                  "%s: 'tiqr_timeout' value is not an integer: %s, using 30 seconds"
                    %(__name__, self.push_timeout))
            max_time = 30
        data = {"user": self.user,
                "transaction_id": transaction_id,
                "pass": ''}

        if self.realm:
            data["realm"] = self.realm

        ### There is 'bug' in the openssh daemon software. That does not show info to the
        # user until PAM_PROMPT_ECHO_OFF/ON is used.
        #
        if self.service in [ 'ssh', 'sshd' ]:
            message = message + ' and hit return to continue'
            max_time = 2
            self.pamh.conversation(self.pamh.Message(self.pamh.PAM_PROMPT_ECHO_OFF, message))
        else:
            self.pamh.conversation(self.pamh.Message(self.pamh.PAM_TEXT_INFO, message))

        # Loop till the user confirms it on the smartphone
        user_time = 0
        while user_time < max_time:
            if self.check_challenge_response(data):
                rval = self.pamh.PAM_SUCCESS
                break
            else:
                time.sleep(2)
                user_time += 2

        return rval

    def push_challenge_response(self, password, transaction_id, message, attributes):
        if self.debug:
            syslog.syslog(syslog.LOG_DEBUG, "Polling for PUSH challenge response")
        return self.check_push_response(password, transaction_id, message)

    def tiqr_challenge_response(self, password, transaction_id, message, attributes):
        qr_data = attributes.get('value')
        if self.debug:
            syslog.syslog(syslog.LOG_DEBUG, "Polling for TIQR challenge response: %s" %(qr_data))

        qr = generate_qr( qr_data)
        self.pamh.conversation(self.pamh.Message(self.pamh.PAM_TEXT_INFO, qr))
        return self.check_push_response(password, transaction_id, message)

    def totp_challenge_response(self, password, transaction_id, message, attributes):
        rval = self.pamh.PAM_AUTH_ERR
        syslog.syslog(syslog.LOG_DEBUG, "TOTP challenge response:")

        pam_mesg = self.pamh.Message(self.pamh.PAM_PROMPT_ECHO_OFF, "%s " %message)
        response = self.pamh.conversation(pam_mesg)
        otp = response.resp

        data = {"user": self.user,
                "transaction_id": transaction_id,
                "pass": otp}
        if self.realm:
            data["realm"] = self.realm

        if self.check_challenge_response(data):
            rval = self.pamh.PAM_SUCCESS

        return rval

    def check_challenge_response(self, data):
        json_response = self.make_request(data)
        result = json_response.get("result")
        detail = json_response.get("detail")

        if self.debug:
            syslog.syslog(syslog.LOG_DEBUG,
                  "%s: result: %s" % (__name__, result))
            syslog.syslog(syslog.LOG_DEBUG,
                  "%s: detail: %s" % (__name__, detail))

        if result.get("status"):
            if result.get("value"):
                return True

        return False


def pam_sm_authenticate(pamh, flags, argv):
    config = _get_config(argv)
    debug = config.get("debug")
    try_first_pass = config.get("try_first_pass")
    prompt = config.get("prompt", "Your token pincode:")
    if prompt[-1] != ":":
        prompt += ":"
    rval = pamh.PAM_AUTH_ERR
    syslog.openlog(facility=syslog.LOG_AUTH)

    Auth = Authenticator(pamh, config)
    try:
        if pamh.authtok is None or not try_first_pass:
            message = pamh.Message(pamh.PAM_PROMPT_ECHO_OFF, "%s " % prompt)
            response = pamh.conversation(message)
            pamh.authtok = response.resp

        if debug and try_first_pass:
            syslog.syslog(syslog.LOG_DEBUG, "%s: running try_first_pass" %
                          __name__)
        rval = Auth.authenticate(pamh.authtok)

        # If the first authentication did not succeed but we have
        # try_first_pass, we ask again for a password:
        if rval != pamh.PAM_SUCCESS and try_first_pass:
            # Now we give it a second try:
            message = pamh.Message(pamh.PAM_PROMPT_ECHO_OFF, "%s " % prompt)
            response = pamh.conversation(message)
            pamh.authtok = response.resp

            rval = Auth.authenticate(pamh.authtok)

    except Exception as exx:
        syslog.syslog(syslog.LOG_ERR, traceback.format_exc())
        syslog.syslog(syslog.LOG_ERR, "%s: %s" % (__name__, exx))
        rval = pamh.PAM_AUTH_ERR
    except requests.exceptions.SSLError:
        syslog.syslog(syslog.LOG_CRIT, "%s: SSL Validation error. Get a valid "
                                       "SSL certificate for your privacyIDEA "
                                       "system. For testing you can use the "
                                       "options 'nosslverify'." % __name__)
    finally:
        syslog.closelog()

    return rval


def pam_sm_setcred(pamh, flags, argv):
    return pamh.PAM_SUCCESS


def pam_sm_acct_mgmt(pamh, flags, argv):
    return pamh.PAM_SUCCESS


def pam_sm_open_session(pamh, flags, argv):
    return pamh.PAM_SUCCESS


def pam_sm_close_session(pamh, flags, argv):
    return pamh.PAM_SUCCESS


def pam_sm_chauthtok(pamh, flags, argv):
    return pamh.PAM_SUCCESS

def generate_qr(data):
    import pyqrcode
    qr = pyqrcode.create( data, error='L' )
    #QRCode.svg('/tmp/QRCode.svg', scale=8)
    #QRCode.eps('/tmp/QRCode.eps', scale=2)
    #print(QRCode.terminal(quiet_zone=1))
    #return str(qr.terminal(quiet_zone=1))
    ## best results for  android  with white background
    return str(qr.terminal(quiet_zone=2, module_color='reverse', background='default'))
